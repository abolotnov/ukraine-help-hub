from django.shortcuts import render
from django.http import JsonResponse


def status(request):
    return JsonResponse({"status": "OK"}, status=200)
