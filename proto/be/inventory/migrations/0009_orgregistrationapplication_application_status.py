# Generated by Django 4.0.3 on 2022-04-04 21:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0008_remove_orgregistrationapplication_admin_name_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='orgregistrationapplication',
            name='application_status',
            field=models.CharField(choices=[('pending', 'Pending Approval'), ('approved', 'Approved'), ('declined', 'Declined')], default='pending', max_length=22),
        ),
    ]
