# Generated by Django 4.0.3 on 2022-04-22 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0010_transoriginal_translation'),
    ]

    operations = [
        migrations.CreateModel(
            name='BarCodeData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=32)),
                ('data', models.JSONField()),
            ],
        ),
    ]
