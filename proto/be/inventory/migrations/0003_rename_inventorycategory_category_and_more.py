# Generated by Django 4.0.3 on 2022-04-02 21:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0002_alter_warehouse_address'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='InventoryCategory',
            new_name='Category',
        ),
        migrations.RenameModel(
            old_name='InventoryDemand',
            new_name='Demand',
        ),
        migrations.RenameModel(
            old_name='InventoryItem',
            new_name='Item',
        ),
        migrations.RenameModel(
            old_name='InventorySubCategory',
            new_name='SubCategory',
        ),
    ]
