# Generated by Django 4.0.3 on 2022-04-02 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_alter_item_code_alter_item_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventoryavailability',
            name='item_color',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='inventoryavailability',
            name='item_size',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='inventoryavailability',
            name='quantity_type',
            field=models.TextField(choices=[('kg', 'Kilogram'), ('ml', 'Milliliter'), ('box', 'Box'), ('item', 'Singleitem'), ('other', 'Other'), ('bottle', 'Bottle')], default='item'),
        ),
        migrations.AlterUniqueTogether(
            name='inventoryavailability',
            unique_together={('item', 'quantity_type', 'item_size', 'item_color')},
        ),
        migrations.AlterUniqueTogether(
            name='item',
            unique_together={('sub_category', 'name')},
        ),
        migrations.RemoveField(
            model_name='item',
            name='item_color',
        ),
        migrations.RemoveField(
            model_name='item',
            name='item_size',
        ),
        migrations.RemoveField(
            model_name='item',
            name='quantity_type',
        ),
    ]
