from .serializers import ItemSerializer, CategorySerializer, SubCategorySerializer, TagSerializer, UserSerializer
from .serializers import ItemAvailabilitySerializer, DemandSerializer
from .serializers import OrgSerializer, WarehouseSerializer
from .serializers import AddressSerializer
from .serializers import OrgRegistrationApplicationSerializer
from .serializers import PubOrgRegistrationApplicationSerializer
from .serializers import UserProfileSerializer
from rest_framework.viewsets import ModelViewSet, ViewSet, mixins, GenericViewSet
from ..models import Item, Category, SubCategory, Tag, ItemAvailability, Demand, Org, Warehouse
from ..models import OrgRegistrationApplication
from ..models import UserProfile, OrgRegistrationStatus
from address.models import Address
import django_filters
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.db import transaction
from .filters import ItemFilterSet
from .metadata import ExtendedMetaDataProvider
from rest_framework.permissions import IsAuthenticated
from django.conf import settings


class UserView(GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['get'], detail=False, url_name='my_profile')
    def my_profile(self, request):
        return Response(UserSerializer(request.user).data)


class OrgRegistrationApplicationViewSet(mixins.CreateModelMixin,
                                        mixins.RetrieveModelMixin,
                                        mixins.ListModelMixin,
                                        GenericViewSet):

    def get_serializer(self, *args, **kwargs):
        if self.action == 'create':
            return PubOrgRegistrationApplicationSerializer(*args, **kwargs)
        return OrgRegistrationApplicationSerializer(*args, **kwargs)

    def get_queryset(self):
        return OrgRegistrationApplication.objects.all()

    @action(methods=['post'], detail=True)
    def decline(self, request, pk):
        try:
            application = OrgRegistrationApplication.objects.get(pk=pk)
            if application.application_status == OrgRegistrationStatus.APPROVED:
                raise AttributeError("The application has already been approved")
            if application.application_status == OrgRegistrationStatus.DECLINED:
                raise AttributeError("The application has been already declined")
            application.application_status = OrgRegistrationStatus.DECLINED
            application.review_notes = request.data.get("review_notes")
            application.save()
            return Response({"ok": "application declined"})
        except (AttributeError, ValueError, Exception) as e:
            return Response({"error": "failed to reject application"})

    @action(methods=['post'], detail=True)
    def approve(self, request, pk):
        application = OrgRegistrationApplication.objects.get(pk=pk)
        try:
            with transaction.atomic():
                if application.application_status == OrgRegistrationStatus.APPROVED:
                    raise AttributeError("This application has already been approved")
                if application.application_status == OrgRegistrationStatus.DECLINED:
                    raise AttributeError("This application has already been declined")
                application.application_status = OrgRegistrationStatus.APPROVED
                application.save()

                _org, _org_created = Org.objects.get_or_create(name=application.name,
                                                               web=application.web,
                                                               email=application.email)
                if not _org_created:
                    raise AttributeError(f"Organization ({_org.id}: {_org.name}) already exists")
                _org.phone = application.phone
                _org.save()

                _user, _user_created = User.objects.get_or_create(email=application.admin_email,
                                                                    username=application.admin_email)
                if not _user_created:
                    raise ValueError("User with this email already exists: one user can only be associated with one organization")
                _user.first_name = application.admin_first_name
                _user.last_name = application.admin_last_name
                _user.email = application.admin_email
                _user.password = User.objects.make_random_password()
                _user.save()

                _profile = UserProfile.objects.create(user=_user, org=_org, is_admin=True)

                return Response({"org": OrgSerializer(_org).data,
                                 "profile": UserProfileSerializer(_profile).data,
                                 "user_password": _user.password}, status=201)
        except (ValueError, AttributeError) as e:
            return Response({"error": e}, status=400)
        except Exception as e:  # noqa Got only knows what could pop here
            return Response({"error": "server error, please contact administrator"}, status=500)


class AddressViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)

    serializer_class = AddressSerializer

    def get_queryset(self):
        return Address.objects.all()


class OrgViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)

    serializer_class = OrgSerializer

    def get_queryset(self):
        return Org.objects.all()


class WarehouseViewSet(ModelViewSet):
    serializer_class = WarehouseSerializer

    def get_queryset(self):
        return Warehouse.objects.all()


class ItemViewSet(ModelViewSet):
    serializer_class = ItemSerializer
    filterset_class = ItemFilterSet
    metadata_class = ExtendedMetaDataProvider

    def get_queryset(self):
        return Item.objects.all()

    @action(methods=['get'], detail=True, url_name='inventory')
    def inventory(self, request, pk):
        data = ItemAvailability.objects.filter(item__pk=pk)
        return Response(ItemAvailabilitySerializer(data, many=True).data, status=200)


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.all()


class SubCategoryViewSet(ModelViewSet):
    serializer_class = SubCategorySerializer

    def get_queryset(self):
        return SubCategory.objects.all()


class TagViewSet(ModelViewSet):
    serializer_class = TagSerializer

    def get_queryset(self):
        return Tag.objects.all()


class ItemAvailabilityViewSet(ModelViewSet):
    serializer_class = ItemAvailabilitySerializer

    def get_queryset(self):
        return ItemAvailability.objects.all()


class DemandViewSet(ModelViewSet):
    serializer_class = DemandSerializer

    def get_queryset(self):
        return Demand.objects.all()
