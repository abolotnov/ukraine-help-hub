from django_filters import rest_framework as filters
from ..models import Item
from django_filters.filters import NumberFilter


class ItemFilterSet(filters.FilterSet):
    category = NumberFilter('sub_category__category__pk', lookup_expr='exact')

    class Meta:
        model = Item
        fields = {
            'name': ['exact', 'contains'],
            'sub_category': ['exact'],
        }
