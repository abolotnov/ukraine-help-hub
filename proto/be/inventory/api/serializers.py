from rest_framework.serializers import ModelSerializer, SerializerMethodField, Serializer
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet
from ..models import Item, Org, Warehouse
from ..models import Category, SubCategory, Tag
from ..models import ItemAvailability, Demand
from ..models import OrgRegistrationApplication
from ..models import UserProfile
from address.models import Address
from django.contrib.auth.models import User


class UserProfileSerializer(ModelSerializer):

    class Meta:
        model = UserProfile
        fields = '__all__'


class UserSerializer(ModelSerializer):
    profile = SerializerMethodField()

    class Meta:
        model = User
        exclude = ['password']

    def get_profile(self, user):  # noqa
        try:
            return UserProfileSerializer(UserProfile.objects.get(user=user)).data
        except UserProfile.DoesNotExist:
            return None


class OrgRegistrationApplicationSerializer(ModelSerializer):
    class Meta:
        model = OrgRegistrationApplication
        fields = '__all__'


class PubOrgRegistrationApplicationSerializer(ModelSerializer):
    class Meta:
        model = OrgRegistrationApplication
        exclude = ['review_notes', 'application_status']


class AddressSerializer(ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class OrgSerializer(ModelSerializer):
    class Meta:
        model = Org
        fields = '__all__'


class WarehouseSerializer(ModelSerializer):
    class Meta:
        model = Warehouse
        fields = '__all__'


class ItemSerializer(ModelSerializer):

    class Meta:
        model = Item
        fields = ['id', 'category', 'sub_category', 'name', 'code', 'tags', 'available_quantity', 'translations']


class SubCategorySerializer(ModelSerializer):
    class Meta:
        model = SubCategory
        fields = '__all__'


class CategorySerializer(ModelSerializer):
    sub_category = SerializerMethodField()
    available_items_count = SerializerMethodField()

    class Meta:
        model = Category
        fields = '__all__'

    def get_sub_category(self, category):
        _subs = SubCategory.objects.filter(category=category)
        return SubCategorySerializer(_subs, many=True).data

    def get_available_items_count(self, category):
        return ItemAvailability.objects.filter(item__sub_category__category__name=category).count()


class TagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class ItemAvailabilitySerializer(ModelSerializer):
    class Meta:
        model = ItemAvailability
        fields = '__all__'


class DemandSerializer(ModelSerializer):
    class Meta:
        model = Demand
        fields = '__all__'

