from rest_framework.metadata import SimpleMetadata
from collections import OrderedDict
from django.utils.encoding import force_str


class ExtendedMetaDataProvider(SimpleMetadata):
    def determine_metadata(self, request, view):
        metadata = super().determine_metadata(request, view)
        filters = OrderedDict()
        if hasattr(view, 'filterset_class'):
            for filter_name, filter_type in view.filterset_class.base_filters.items():
                filters.update({filter_name: filter_type.lookup_expr})
        if hasattr(view, 'filter_class'):
            filters.update({'filter_class': 'not_implemented'})
        metadata['filters'] = filters
        return metadata
"""
        for filter_name, filter_type in view.filter_class.base_filters.items():
            filter_parts = filter_name.split('__')
            filter_name = filter_parts[0]
            attrs = OrderedDict()

            # Type
            attrs['type'] = filter_type.__class__.__name__

            # Lookup fields
            if len(filter_parts) > 1:

                # Has a lookup type (__gt, __lt, etc.)
                lookup_type = filter_parts[1]
                if filters.get(filter_name) is not None:

                    # We've done a filter with this name previously, just append the value.
                    attrs['lookup_types'] = filters[filter_name]['lookup_types']
                    attrs['lookup_types'].append(lookup_type)
                else:
                    attrs['lookup_types'] = [lookup_type]
            else:

                # Exact match or RelatedFilter
                #if isinstance(filter_type, RelatedFilter):
                #    model_name = (filter_type.filterset.Meta.model._meta.verbose_name_plural.title()) # noqa
                #    attrs['lookup_types'] = "See available filters for '%s'" % model_name
                #else:
                attrs['lookup_types'] = ['exact']

            # Do choices
            choices = filter_type.extra.get('choices', False)
            if choices:
                attrs['choices'] = [
                    {
                        'value': choice_value,
                        'display_name': force_str(choice_name, strings_only=True)
                    }
                    for choice_value, choice_name in choices
                ]

            # Wrap up.
            filters[filter_name] = attrs

        metadata['filters'] = filters

        if hasattr(view, 'ordering_fields'):
            metadata['ordering'] = view.ordering_fields
        return metadata
        """



