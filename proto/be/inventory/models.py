import os

from django.core.validators import RegexValidator
from django.db import models
from address.models import AddressField
from phone_field.models import PhoneField
from django.contrib.auth.models import User
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from google.cloud import translate_v2 as translate
from django.conf import settings
import logging
from django.db.models import options
options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('translate_fields', )


class DayOfTheWeek(models.TextChoices):
    Monday = 'M'
    Tuesday = 'T'
    Wednesday = 'W'
    Thursday = 'Th'
    Friday = 'F'
    Saturday = 'Sat'
    Sunday = 'S'


class OrgRegistrationStatus(models.TextChoices):
    PENDING_APPROVAL = 'pending',
    APPROVED = 'approved',
    DECLINED = 'declined'


class QuantityMeasure(models.TextChoices):
    Kilogram = 'kg',
    Milliliter = 'ml',
    Box = 'box',
    SingleItem = 'item',
    Other = 'other',
    Bottle = 'bottle'


class ItemStatus(models.TextChoices):
    CreatedBySystem = 'created by system',
    FromDemand = 'created from demand',
    FromAvailability = 'created from delivered goods',
    Approved = 'reviewed and approved',
    Deleted = 'deleted'


class TranslatableModel(models.Model):

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print(f"initializing self id {self.id} name {self.name} attr {getattr(self, 'name')}")
        if not self._meta.__getattribute__("translate_fields"):
            raise ValueError('translate_fields missing in Meta class')
        if not isinstance(self._meta.__getattribute__("translate_fields"), list):
            raise ValueError('translate_fields must be a list')

    @property
    def translations(self, *args):
        data = list()
        for f in self._meta.__getattribute__("translate_fields"):
            print(getattr(self, "name"))
            translation = TransOriginal.translations(getattr(self, f))
            if translation:
                translation.update({"field": f})
                data.append(translation)
            else:
                continue
        return data

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for f in self._meta.__getattribute__("translate_fields"):
            TranslatableModel.translate_item_name(self.__getattribute__(f))

    @staticmethod
    def translate_item_name(term):
        if not os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
            logging.warning("No GOOGLE_APPLICATION_CREDENTIALS environment variable - skipping translation")
            return

        orig, created = TransOriginal.objects.get_or_create(text=term)
        if not created:
            logging.warning(f"Term {term} exists in translation tables, aborting translation")
            return

        client = translate.Client()
        for code in settings.TRANSLATION_LANG_CODES:
            result = client.translate(term, target_language=code)
            Translation.objects.create(original=orig, text=result["translatedText"], lang_code=code)
        logging.info(f"Translated {term} into {result['translatedText']}")


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    org = models.OneToOneField('Org', on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)


class Org(models.Model):
    name = models.CharField(max_length=128, unique=True, blank=False, null=False)
    web = models.URLField(blank=True, null=True)
    phone = PhoneField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    active = models.BooleanField(default=False)
    screened = models.BooleanField(default=False)
    managers = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class OrgRegistrationApplication(models.Model):
    name = models.CharField(max_length=256, blank=False, null=False)
    web = models.URLField(max_length=256, blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                    message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(max_length=15, blank=False, null=False, validators=[phone_regex])
    email = models.EmailField(null=False, blank=False)
    date_submitted = models.DateTimeField(auto_now_add=True)
    admin_first_name = models.CharField(max_length=128, null=False, blank=False, default='unknown')
    admin_last_name = models.CharField(max_length=128, null=True, blank=True, default='unknown')
    admin_email = models.EmailField(null=False, blank=False)
    admin_phone = models.CharField(max_length=15, null=False, blank=False, validators=[phone_regex])
    application_status = models.CharField(max_length=22, null=False, blank=False,
                                          choices=OrgRegistrationStatus.choices,
                                          default=OrgRegistrationStatus.PENDING_APPROVAL)
    notes = models.TextField(null=True, blank=True)
    review_notes = models.TextField(null=True, blank=True)


class OperatingHours(models.Model):
    day = models.TextField(choices=DayOfTheWeek.choices, null=False, blank=False)
    start = models.TimeField(null=False, blank=False)
    end = models.TimeField(null=False, blank=False)
    site = models.ForeignKey('Warehouse', on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        return f"{self.day}: {self.start} - {self.end}"


class Warehouse(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False, unique=True)
    org = models.ForeignKey(Org, on_delete=models.RESTRICT)
    address = AddressField(blank=True, null=True)  # disabled address for now see django-address  tests on how it works
    phone = PhoneField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)

    def __str__(self):
        return f"{self.name} at {self.address}"


class Tag(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        super().save(*args, **kwargs)


class Category(models.Model):
    name = models.CharField(max_length=256, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False, blank=False)
    name = models.CharField(max_length=256, null=False, blank=False)

    class Meta:
        unique_together = ['category', 'name']

    def __str__(self):
        return f"{self.category} > {self.name}"


class ItemAvailability(models.Model):
    item = models.ForeignKey('Item', on_delete=models.RESTRICT, null=False, blank=False)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.RESTRICT, null=False, blank=False)
    quantity_type = models.TextField(choices=QuantityMeasure.choices, null=False, blank=False,
                                     default=QuantityMeasure.SingleItem)
    item_size = models.CharField(max_length=32, null=True, blank=True)
    item_color = models.CharField(max_length=32, null=True, blank=True)
    quantity = models.IntegerField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['item', 'quantity_type', 'item_size', 'item_color']

    def __str__(self):
        return f"{self.item}: {self.quantity}"

    def get_releases(self):
        return InventoryRelease.objects.filter(item__pk=self.pk)


class InventoryRelease(models.Model):
    item = models.ForeignKey(ItemAvailability, on_delete=models.RESTRICT, null=False, blank=False)
    quantity = models.IntegerField(null=False, blank=False)
    date_released = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.quantity > self.item.item.available_quantity:
            raise ValueError("Release quantity > available quantity, please update Inventory first or reduce quantity")
        _availability = ItemAvailability.objects.get(pk=self.item.pk)
        _availability.quantity -= self.quantity
        _availability.save()
        super().save(*args, **kwargs)


class Item(TranslatableModel):
    sub_category = models.ForeignKey(SubCategory, null=False, blank=False, on_delete=models.RESTRICT)
    name = models.CharField(max_length=128, null=False, blank=False)
    barcode = models.CharField(max_length=64, null=True, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    status = models.CharField(max_length=128, null=False, blank=False, choices=ItemStatus.choices,
                              default=ItemStatus.CreatedBySystem)

    class Meta:
        unique_together = ['sub_category', 'name']
        translate_fields = ['name']

    def __str__(self):
        return f"{self.sub_category.category} > {self.sub_category}: {self.name}"

    @property
    def available_quantity(self):
        _quantity = ItemAvailability.objects.filter(item__pk=self.pk).aggregate(Sum('quantity'))['quantity__sum']
        return _quantity if _quantity else 0

    def get_available_inventory(self):
        return ItemAvailability.objects.filter(item__pk=self.pk)

    @property
    def category(self):
        return self.sub_category.category.pk


class Demand(models.Model):
    item = models.ForeignKey(Item, on_delete=models.RESTRICT, null=False, blank=False)
    quantity_type = models.TextField(choices=QuantityMeasure.choices, null=False, blank=False)
    quantity = models.IntegerField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return f"{self.item}: {self.quantity} {self.quantity_type}"


class TransOriginal(models.Model):
    text = models.TextField(null=False, blank=False, unique=True)

    @staticmethod
    def translations(term):
        try:
            _orig = TransOriginal.objects.get(text=term)
            _data = dict()
            for item in Translation.objects.filter(original=_orig):
                _data.update({"lang_code": item.lang_code, "text": item.text})
            return _data
        except TransOriginal.DoesNotExist:
            return None


class Translation(models.Model):
    original = models.ForeignKey(TransOriginal, on_delete=models.DO_NOTHING)
    lang_code = models.CharField(max_length=4)
    text = models.TextField()


class BarCodeData(models.Model):
    code = models.CharField(max_length=32, null=False, blank=False)
    data = models.JSONField()
