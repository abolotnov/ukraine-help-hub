import logging
import os
import requests
import string

from django.conf import settings
from .models import BarCodeData


def get_barcode_info(code):
    if not settings.UPC_API_KEY:
        logging.error("no UPC_API_KEY set - not going to fetch data from barcode server")
        return None
    if not 11 < len(code) < 15:
        raise ValueError("bar code len should be between 12 and 14 digits")
    if string.ascii_letters in code:
        raise ValueError("bar code can only contain digits")

    try:
        barcode_data = BarCodeData.objects.get(code=code)
        logging.info(f"found data for code {code} in the database")
        return barcode_data.data
    except BarCodeData.DoesNotExist:
        response = requests.get(os.path.join(settings.UPC_BASE_URL, code),
                                headers={"Authorization": f"Basic {settings.UPC_API_KEY}"},
                                params={"apikey": settings.UPC_API_KEY})
        logging.info(f"got barcode data: {response.text}")
        if response.json().get("success"):
            BarCodeData.objects.create(code=code, data=response.json())
            return response.json()
        else:
            return None
