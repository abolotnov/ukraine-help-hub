# Why & what
The goal of the project is to build a hub allowing multiple non-profit organizations (NPOs) to centralize their demand, inventory and case management to move inventory from helping countries and deliver help where it's needed.

See `docs` folder for documentation/drawings
See `proto` folder for an attempt to implement a basic inventory/demand API that would allow systems to plug in. The current idea is to implement a REST API backend and build the UI with rapid prototyping tools/applications builders, such as `unqork`, `budibase` and others.

# Concept

We're building an inventory system for "B2B" style of operations (between vetted domor and recipient organizations) that would be supported by a simple case management system to allow tracking delivery and registration of inventory (this requires aligning wareshouses space availability and volunteers to deliver, unload and register inventory). Eventually, the case management system would be something organizations will use to manage their "retail" cases when struggling people call/email for help and they need to deliver or post items.

## Inventory system

Inventory system that will allow keeping a catalogue of available inventory and its location and also book demand for specific inventory that's not currently available. Initial focus is with "B2B" type of operation where vetted organizations raise demand for inventory and register it once its recieved. We need to be able to translate data to multiple languages and ideally enter inventory via barcode.

## Case management system

Not sure if this is something we want to code from scratch - ideally adopt some open source but need to be able to deal with our specifics:

- B2B cases management
- Retail cases
- Complex cases (individual looking for items, they are not in inventory - demand booked - org alerted on available demand - items are packaged and delivered) etc.

# Help wanted
We need help from the engineering community to prototype and launch this quickly:

- Python engineers, familar with django and DRF
- Folks familiar with no/low code engineering tools to build UI (we're trying to make some progress with appSmith here: `https://gitlab.com/abolotnov/ukraine-help-hub-appsmith`)
- Analysts to help structure the models for the inventory and demand management
- Folks who can discover and adapt a case management system to integrate with it
- Folks able to build test data and start automating integration tests

`abolotnov@gmail.com` - feel free to email if you have thoughts/suggestions. You can also join [telegram group](https://t.me/+pz7Q-Bu7N3I4ZDlh) to ask your questions real time.
